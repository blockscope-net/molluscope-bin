# 🦑 Molluscope - Subsquid CLI

A monitoring CLI tool for Subsquid worker nodes.

![Molluscope banner](software-ad-sui-molluscope.png)

# 🗒 Description

Molluscope is a monitoring and alerting tool for Subsquid worker nodes, it aims to be simple to install and use, and useful in features provided.

Molluscope displays your workers and network info via several communication channels: Discord, Telegram, PagerDuty and Slack. It also provides commands to get workers info on demand on the shell.

Features:

- Get info about: workers, network, and epochs
- Alerts for workers: uptime, delegations, jail, active, and more
- Real time monitoring on the shell
- Integrates with: Discord, Telegram, PagerDuty and Slack

# 📦 Installation

### **Install dependencies**

```sh
sudo apt install git -y
```

### **Clone the repository**

```sh
cd ~
git clone https://gitlab.com/blockscope-net/molluscope.git
cd molluscope
git pull

# copy the corresponding binary to a system folder set in the PATH
cp molluscope-apple-silicone /usr/local/bin/moll
```

# ⚙️ Config

Next step is configuring Molluscope via its config yaml file. Copy the config file template to your home directory and edit it to change the custom values for your validator account.

```sh
cp .molluscope-tpl.json ~/.molluscope.json
vim ~/.molluscope.json
```

Find your workers id with the get command.

```sh
moll get -l
```

Edit config file values, specially workerIds and communication channels (leave fields empty "" for disabling it).

```yaml
{
  "rpc": "https://subsquid.squids.live/subsquid-network-testnet/v/v1/graphql",
  "workerIds": [YOUR_WORKERS_ID_COMMA_SEPARATED],
  "discord":
    {
      "username": "Molluscope Testnet",
      "url": "https://discord.com/api/webhooks/....webhook_id",
    },
  "telegram":
    { "botId": "....telegram_bot_id", "chatId": "....telegram_chat_id" },
  "pagerduty": { "integrationKey": "....pagerduty_integration_key" },
  "slack":
    {
      "oauthToken": ....slack_oauth_token",
      "channelId": "....slack_channel_id",
    },
  "monitor": { "pingInteral": 3600, "catchupInterval": 1, "alertMaxAlerts": 5 },
}
```

# ⌨️ Usage

First you need to know is that all the commands have a help you can query with the -h or --help flag.

## Commands help

```sh
# display general moll help
moll --help
# display coin command help
moll get -h
```

## Get command

Used to manage Molluscope configuration.

```sh
# get the info of the workers in the config file (workerIds)
moll get

# get the info of the worker with id = 100
moll get -w 100

# get the info of the network
moll get -n

# get the info of the latest epochs
moll get -e
```

## Monitor command

Used to monitor workers and send alerts on performance and delegation events.

```sh
# monitor workers directly on the shell
moll monitor

# monitor workers directly on the shell and send alerts to the configured communication channels
moll monitor -w
```

## 😈 Run with a Service

As a monitoring tool it's better to handle its execution with a Linux service, so it starts on boot/reboots and restarts in the case it stops.

### **Create service file**

```sh
MOLLUSCOPE_PATH=/usr/local/bin/

echo "[Unit]
Description=Molluscope Service

[Service]
User="$USER"
Group="$USER"
Environment=PATH=$PATH:$MOLLUSCOPE_PATH
ExecStart="$MOLLUSCOPE_PATH/moll monitor -w"
WorkingDirectory="$MOLLUSCOPE_PATH"
Restart=on-failure
RestartSec=5s

[Install]
WantedBy=multi-user.target " > molluscope.service
```

### **Move service file to services location**

```sh
sudo mv ./molluscope.service /etc/systemd/system/molluscope.service
```

### **Enable the service**

```sh
sudo systemctl daemon-reload
sudo systemctl enable molluscope
```

### **Run the service**

```sh
sudo systemctl start molluscope
```

### **Check logs / output**

```sh
sudo journalctl -u molluscope -fo cat
```

### **Stop the service**

```sh
sudo systemctl stop molluscope
```

### **Check service status**

```sh
sudo systemctl status molluscope
```

## 👀 Visuals

### Worker Shell Monitoring

![Shell Monitoring](moll-ss-monitor-shell.png)

### Alerts (Telegram, Discord, PagerDuty)

![Alerts Telegram](moll-ss-telegram-alerts.png)

![Alerts Discord](moll-ss-discord-alerts.png)

![Alerts PagerDuty](moll-ss-pagerduty-alerts.png)

### Workers List

![Workers List](moll-ss-get-list.png)

### Worker Info

![Worker Info](moll-ss-get-worker.png)

### Network Info

![Network Info](moll-ss-get-network.png)

### Epochs List

![Epochs List](moll-ss-get-epochs.png)

# 🤔 Troubleshooting

- Why doesn't my shell show the Molluscope UI colors?
  Probably your shell is not in 256 color mode. You can set that in your .bash_profile (or correspoding shell config file)

In bash:

```
export TERM=xterm-256color
```

In zsh:

```
export TERM=screen-256color
```

If your using Tmux and the problem persist, check this thread to config your Tmux https://unix.stackexchange.com/questions/1045/getting-256-colors-to-work-in-tmux

# 📨 Support

Send any related questions to blockscope@protonmail.com

# 💻 Contributing

Feel free to propose any node operations you want to be included in Molluscope 🦑.

# 🛣️ Roadmap

Release v0.0.1 (25th March 2024)

- First version
- Get info command for workers, network and epochs
- Monitor command for worker status
- Worker alerts
- Integration with Discord, Telegram, Slack and PagerDuty

# 🏗️ Project status

Alive! We keep updating the project with bug fixes and improvements, don't hesitate to submit an issue to the repository.

# ✍️ Authors

Blockscope.net :: https://blockscope.net :: {📦}
